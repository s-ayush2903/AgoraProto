# AgoraProto

# This is the **[Prototype](https://xd.adobe.com/view/dcc2ef12-aa63-49e2-56a8-78f071656e19-aaf3/)** developed using XD

MockUp Repo for AgoraAndroid

**Auth Sreens**

![Auth Screen](https://gitlab.com/aossie/agora-android/uploads/644641105c5e6de300a1888bbbdfb457/LogIn.png)                  ![](https://gitlab.com/aossie/agora-android/uploads/afde5c084c2339e3863f59e2f7023951/SignUp.png)

![](https://gitlab.com/aossie/agora-android/uploads/c2a31e7d04db1e175f3f9361c51fc5d1/Forgot_Password.png)                   ![](https://gitlab.com/aossie/agora-android/uploads/1cb960e3c8e4b2266d1a2b886d663459/TwoFactorAuth.png)


**Create Elections(xml Prepared)**

![](https://gitlab.com/aossie/agora-android/uploads/3fd16ab3cd70c17655013496a36187c3/CreateElection1.png)                   ![](https://gitlab.com/aossie/agora-android/uploads/8cbf3177ccc9a56bc6c64f1b35b9a120/CreateElection2.png)

![](https://gitlab.com/aossie/agora-android/uploads/592e1d83c879d4bc09ddcf7e689bbafe/CreateElection3.png)                   ![](https://gitlab.com/aossie/agora-android/uploads/fc9a0b6764fca4308147f949b8dda087/CreateElection4.png)

![](https://gitlab.com/aossie/agora-android/uploads/6ec072a90b68f438ecc31719028aeeeb/ElectionCreated.png)                   ![](https://gitlab.com/aossie/agora-android/uploads/8105213f994446c5368201febaae42ba/InviteVoters.png)

![](https://gitlab.com/aossie/agora-android/uploads/4b084e0013ef8e4c69a9448cc71c4418/VotersInvited.png)                     ![](https://gitlab.com/aossie/agora-android/uploads/aaa33834b38bd6065f43e8c3ed2b2b9a/TotalElections.png)


**Nav Drawer & Dashboard**

![](https://gitlab.com/aossie/agora-android/uploads/2625ab4cdda9381469f817ce51093504/NavDrawer.png)                         ![](https://gitlab.com/aossie/agora-android/uploads/3c37ff7ac61a286e553167ffa00376fd/Dashboard.png)

**NavComponents**

![](https://gitlab.com/aossie/agora-android/uploads/81341889f184cdbc898063acb0af14d3/EditProfile.png)                       ![](https://gitlab.com/aossie/agora-android/uploads/315ecce83a09a11072cf9ffce5758733/ContactUs.png)

![](https://gitlab.com/aossie/agora-android/uploads/e1e4616b5378c946a48182ffc4f0007c/ShareWithFriends.png)                  ![](https://gitlab.com/aossie/agora-android/uploads/46c8af1b41e3a2ebbb5a5561111378b4/ReportAbug.png)

**No Internet Screeen**

![](https://gitlab.com/aossie/agora-android/uploads/6ff983bf357c98da379a2e4966c0e9ab/NoInternet.png)