package com.example.android.agoraprototype.ui.gallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GalleryViewModel : ViewModel() {
  private val mText: MutableLiveData<String>
  val text: LiveData<String>
    get() = mText

  init {
    mText = MutableLiveData()
    mText.value = "This is gallery fragment"
  }
}