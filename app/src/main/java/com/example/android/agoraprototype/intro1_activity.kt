package com.example.android.agoraprototype

import android.app.Activity
import android.app.AppComponentFactory
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class intro1_activity : AppCompatActivity() {
  private val _bg__intro1_ek2: View? = null
  private val next_ek2: TextView? = null
  private val welcome_to_agora_android__this_is_just_a_short_tour_to_familiarize_you_with_the_functionality_of_the_module: TextView? =
    null
  private val skip_ek2: TextView? = null
  private val rectangle_68_ek2: View? = null
  private val rectangle_69_ek2: View? = null
  private val onboarding_ek2: TextView? = null
  private val path_10_ek2: ImageView? = null
  private val path_237: ImageView? = null
  private val ellipse_10_ek2: View? = null
  private val ellipse_11_ek2: View? = null
  private val ellipse_74: View? = null
  private val agora_ek2: ImageView? = null
  private val path_238_ek2: ImageView? = null
  private val path_239_ek2: ImageView? = null
  public override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.intro1)


//		_bg__intro1_ek2 = (View) findViewById(R.id._bg__intro1_ek2);
//		next_ek2 = (TextView) findViewById(R.id.next_ek2);
//		welcome_to_agora_android__this_is_just_a_short_tour_to_familiarize_you_with_the_functionality_of_the_module = (TextView) findViewById(R.id.welcome_to_agora_android__this_is_just_a_short_tour_to_familiarize_you_with_the_functionality_of_the_module);
//		skip_ek2 = (TextView) findViewById(R.id.skip_ek2);
//		rectangle_68_ek2 = (View) findViewById(R.id.rectangle_68_ek2);
//		rectangle_69_ek2 = (View) findViewById(R.id.rectangle_69_ek2);
//		onboarding_ek2 = (TextView) findViewById(R.id.onboarding_ek2);
//		path_10_ek2 = (ImageView) findViewById(R.id.path_10_ek2);
//		path_237 = (ImageView) findViewById(R.id.path_237);
//		ellipse_10_ek2 = (View) findViewById(R.id.ellipse_10_ek2);
//		ellipse_11_ek2 = (View) findViewById(R.id.ellipse_11_ek2);
//		ellipse_74 = (View) findViewById(R.id.ellipse_74);
//		agora_ek2 = (ImageView) findViewById(R.id.agora_ek2);
//		path_238_ek2 = (ImageView) findViewById(R.id.path_238_ek2);
//		path_239_ek2 = (ImageView) findViewById(R.id.path_239_ek2);


    //custom code goes here
  }
}