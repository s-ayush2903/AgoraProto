package com.example.android.agoraprototype

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class intro3_activity : AppCompatActivity() {
  private val _bg__intro3_ek2: View? = null
  private val lorem_dolor_sit_amet_consectetur_adipisicing_elit__sed_do_eiusmod_tempor_incididunt_ut_ero_labore_et_dolore: TextView? =
    null
  private val ellipse_8_ek1: View? = null
  private val ellipse_9_ek1: View? = null
  private val ellipse_10_ek1: View? = null
  private val ellipse_11_ek1: View? = null
  private val rectangle_68_ek1: View? = null
  private val rectangle_69_ek1: View? = null
  private val onboarding_ek1: TextView? = null
  private val path_10_ek1: ImageView? = null
  private val next_ek1: TextView? = null
  private val skip_ek1: TextView? = null
  private val path_238_ek1: ImageView? = null
  private val path_239_ek1: ImageView? = null
  private val agora_ek1: ImageView? = null
  public override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.intro3)

//
//		_bg__intro3_ek2 = (View) findViewById(R.id._bg__intro3_ek2);
//		lorem_dolor_sit_amet_consectetur_adipisicing_elit__sed_do_eiusmod_tempor_incididunt_ut_ero_labore_et_dolore = (TextView) findViewById(R.id.lorem_dolor_sit_amet_consectetur_adipisicing_elit__sed_do_eiusmod_tempor_incididunt_ut_ero_labore_et_dolore);
//		ellipse_8_ek1 = (View) findViewById(R.id.ellipse_8_ek1);
//		ellipse_9_ek1 = (View) findViewById(R.id.ellipse_9_ek1);
//		ellipse_10_ek1 = (View) findViewById(R.id.ellipse_10_ek1);
//		ellipse_11_ek1 = (View) findViewById(R.id.ellipse_11_ek1);
//		rectangle_68_ek1 = (View) findViewById(R.id.rectangle_68_ek1);
//		rectangle_69_ek1 = (View) findViewById(R.id.rectangle_69_ek1);
//		onboarding_ek1 = (TextView) findViewById(R.id.onboarding_ek1);
//		path_10_ek1 = (ImageView) findViewById(R.id.path_10_ek1);
//		next_ek1 = (TextView) findViewById(R.id.next_ek1);
//		skip_ek1 = (TextView) findViewById(R.id.skip_ek1);
//		path_238_ek1 = (ImageView) findViewById(R.id.path_238_ek1);
//		path_239_ek1 = (ImageView) findViewById(R.id.path_239_ek1);
//		agora_ek1 = (ImageView) findViewById(R.id.agora_ek1);
//

    //custom code goes here
  }
}