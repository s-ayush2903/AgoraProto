package com.example.android.agoraprototype

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class CreateElection2Fragment : Fragment() {
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_create_election2, container, false)
  } /*		_bg__createelection2_ek2 = (View) findViewById(R.id._bg__createelection2_ek2);
		path_36 = (ImageView) findViewById(R.id.path_36);
		rectangle_1467 = (View) findViewById(R.id.rectangle_1467);
		rectangle_68_ek3 = (View) findViewById(R.id.rectangle_68_ek3);
		rectangle_69_ek3 = (View) findViewById(R.id.rectangle_69_ek3);
		path_10_ek6 = (ImageView) findViewById(R.id.path_10_ek6);
		path_36_ek1 = (ImageView) findViewById(R.id.path_36_ek1);
		rectangle_1464 = (View) findViewById(R.id.rectangle_1464);
		undraw_conference_speaker_6nt7 = (ImageView) findViewById(R.id.undraw_conference_speaker_6nt7);
		create_election = (TextView) findViewById(R.id.create_election);
		add_candidates = (TextView) findViewById(R.id.add_candidates);
		rectangle_147_ek10 = (View) findViewById(R.id.rectangle_147_ek10);
		johndoe = (TextView) findViewById(R.id.johndoe);
		candidate_s_name = (TextView) findViewById(R.id.candidate_s_name);
		add = (TextView) findViewById(R.id.add);
		path_240 = (ImageView) findViewById(R.id.path_240);
		rectangle_1032 = (View) findViewById(R.id.rectangle_1032);
		rectangle_1033 = (View) findViewById(R.id.rectangle_1033);
		path_241 = (ImageView) findViewById(R.id.path_241);
		path_238_ek4 = (ImageView) findViewById(R.id.path_238_ek4);
		path_239_ek4 = (ImageView) findViewById(R.id.path_239_ek4);
	*/
  //custom code goes here
}